package pl.awsm.spring.aop.common;

import org.springframework.stereotype.Service;

@Service
public class ExampleService {

	@PrintLog(value = "Before hello")
	public void sayHello() {
		System.out.println("Hello!");
	}

	public void sayGoodbye() {
		System.out.println("Goodbye!");
	}

}
