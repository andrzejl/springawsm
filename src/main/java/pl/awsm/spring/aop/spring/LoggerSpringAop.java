package pl.awsm.spring.aop.spring;

import java.lang.reflect.Method;

import org.springframework.aop.MethodBeforeAdvice;

import pl.awsm.spring.aop.common.PrintLog;

public class LoggerSpringAop implements MethodBeforeAdvice{

	@Override
	public void before(Method method, Object[] args, Object target) throws Throwable {
		if(method.getAnnotation(PrintLog.class)!=null) {
			System.out.println(method.getAnnotation(PrintLog.class).value());
		}
	}

}
