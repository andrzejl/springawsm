package pl.awsm.spring.aop.spring;

import org.springframework.aop.framework.ProxyFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import pl.awsm.spring.aop.common.ExampleService;

public class Main {

	public static void main(String[] args) {
		try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				"pl.awsm.spring.aop.common", "pl.awsm.spring.aop.spring")) {
			ExampleService exampleService = context.getBean(ExampleService.class);
			ProxyFactory pf = new ProxyFactory(exampleService);
			pf.addAdvice(new LoggerSpringAop());
			((ExampleService) pf.getProxy()).sayHello();
			((ExampleService) pf.getProxy()).sayGoodbye();
		}
	}
}
