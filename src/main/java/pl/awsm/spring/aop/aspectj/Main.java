package pl.awsm.spring.aop.aspectj;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import pl.awsm.spring.aop.common.ExampleService;

public class Main {

	public static void main(String[] args) {
		try (AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				"pl.awsm.spring.aop.common", "pl.awsm.spring.aop.aspectj")) {
			ExampleService exampleService = context.getBean(ExampleService.class);
			exampleService.sayHello();
			exampleService.sayGoodbye();
		}

	}

}
