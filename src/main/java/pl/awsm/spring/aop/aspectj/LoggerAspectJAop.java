package pl.awsm.spring.aop.aspectj;

import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import pl.awsm.spring.aop.common.PrintLog;

@Aspect
@Component
public class LoggerAspectJAop {

	@Pointcut("@annotation(pl.awsm.spring.aop.common.PrintLog)")
	public void loggableMethods() {
	}

	@Before("loggableMethods()")
	public void logMethod(JoinPoint joinPoint) {
		MethodSignature signature = (MethodSignature) joinPoint.getSignature();
		Method method = signature.getMethod();
		System.out.println(method.getAnnotation(PrintLog.class).value());
	}

}
