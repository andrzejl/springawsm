package pl.awsm.spring.di_1.xml_2;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import pl.awsm.spring.di_1.constructor_1.Prophet;

public class Main {

	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("di_1/beans.xml");
		Prophet prophet = (Prophet) context.getBean("prophet");
		System.out.println(prophet);
		((ConfigurableApplicationContext)context).close();
	}

}
