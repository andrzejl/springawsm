package pl.awsm.spring.di_1.autowiring_3;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	public static void main(String[] args) {
		
		ApplicationContext context = new ClassPathXmlApplicationContext("di_1/beans_autowiring.xml");
		Prophet prophet = (Prophet) context.getBean("prophet");
		System.out.println(prophet);
		((ConfigurableApplicationContext)context).close();
	}

}
