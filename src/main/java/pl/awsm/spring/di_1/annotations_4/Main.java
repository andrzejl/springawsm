package pl.awsm.spring.di_1.annotations_4;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Main {

	public static void main(String[] args) {
		
		//ApplicationContext context = new ClassPathXmlApplicationContext("di_1/beans_annotations.xml");
		ApplicationContext context = new AnnotationConfigApplicationContext("pl.awsm.spring.di_1.annotations_4");
		Prophet prophet = (Prophet) context.getBean("prophet");
		System.out.println(prophet);
		((ConfigurableApplicationContext)context).close();
	}

}
