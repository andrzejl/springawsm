package pl.awsm.spring.di_1.annotations_4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Prophet {
	
	@Autowired
	private Mountain mountain;

	public Prophet() {
	}

	public Prophet(Mountain mountatin) {
		this.mountain = mountatin;
	}

	public Mountain getMountain() {
		return mountain;
	}

	public void setMountain(Mountain mountatin) {
		this.mountain = mountatin;
	}
	
	@Override
	public String toString() {
		return "I'm a prophet and I have a mountation called: " + mountain.getName();
	}
}
