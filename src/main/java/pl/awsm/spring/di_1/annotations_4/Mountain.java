package pl.awsm.spring.di_1.annotations_4;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Mountain {
	@Value("Annotation Mountain")
	private String name;

	public Mountain() {
	}

	public Mountain(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
