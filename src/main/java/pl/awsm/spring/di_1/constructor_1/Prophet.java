package pl.awsm.spring.di_1.constructor_1;

public class Prophet {
	
	private Mountain mountain;

	public Prophet() {
	}

	public Prophet(Mountain mountatin) {
		this.mountain = mountatin;
	}

	public Mountain getMountain() {
		return mountain;
	}

	public void setMountain(Mountain mountatin) {
		this.mountain = mountatin;
	}
	
	@Override
	public String toString() {
		return "I'm a prophet and I have a mountation called: " + mountain.getName();
	}
}
