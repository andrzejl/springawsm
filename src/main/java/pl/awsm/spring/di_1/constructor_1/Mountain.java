package pl.awsm.spring.di_1.constructor_1;

public class Mountain {
	private String name;

	public Mountain() {
	}

	public Mountain(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
