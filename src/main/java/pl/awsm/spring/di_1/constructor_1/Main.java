package pl.awsm.spring.di_1.constructor_1;

public class Main {

	public static void main(String[] args) {
		
		Prophet prophet = new Prophet(new Mountain("Everest"));
		System.out.println(prophet);
	}

}
