package pl.awsm.spring.di.aplikacja;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import pl.awsm.spring.di.rezerwaty.impl.RezerwatDzikichPsow;

@Configuration
@ComponentScan(basePackages="pl.awsm.spring.di.fabryki.impl")
public class RezerwatDzikichPsowConfig {
	
	@Bean(name="rezerwatDzikichPsow")
	public RezerwatDzikichPsow getRdp() { return new RezerwatDzikichPsow(); }

}
