package pl.awsm.spring.di.aplikacja;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import pl.awsm.spring.di.rezerwaty.impl.RezerwatDzikichMalp;
import pl.awsm.spring.di.rezerwaty.impl.RezerwatDzikichPsow;

public class Aplikacja {

	public static void main(String[] args) {
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("spring-config.xml");
		
		RezerwatDzikichMalp rdm = (RezerwatDzikichMalp) ctx.getBean("pl.awsm.spring.di.rezerwaty.RezerwatDzikichStworzen");
		RezerwatDzikichPsow rdp = (RezerwatDzikichPsow) ctx.getBean("rezerwatDzikichPsow");
		
		rdm.przyjmij();
		rdm.przyjmij();
		rdp.przyjmij();
		
		((AbstractApplicationContext) ctx).close();
	}

}

