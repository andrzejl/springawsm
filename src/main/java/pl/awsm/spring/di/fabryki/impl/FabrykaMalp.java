package pl.awsm.spring.di.fabryki.impl;

import java.util.List;
import java.util.Random;

import pl.awsm.spring.di.fabryki.FabrykaDzikichStworzen;

public class FabrykaMalp implements FabrykaDzikichStworzen{
	
	public List<String> katalogMalp;

	public void setKatalogMalp(List<String> malpy) {
		this.katalogMalp = malpy;
	}

	@Override
	public String wyprodukuj() {
		Random rand = new Random();
		return katalogMalp.get(rand.nextInt(katalogMalp.size()));
	}
}
