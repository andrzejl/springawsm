package pl.awsm.spring.di.fabryki.impl;

import org.springframework.stereotype.Component;

import pl.awsm.spring.di.fabryki.FabrykaDzikichStworzen;

@Component
public class FabrykaPsow implements FabrykaDzikichStworzen {

	@Override
	public String wyprodukuj() {
		return "Floki";
	}

}
