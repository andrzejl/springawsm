package pl.awsm.spring.di.rezerwaty.impl;

import org.springframework.beans.factory.annotation.Autowired;

import pl.awsm.spring.di.fabryki.impl.FabrykaPsow;
import pl.awsm.spring.di.rezerwaty.RezerwatDzikichStworzen;

public class RezerwatDzikichPsow implements RezerwatDzikichStworzen {
	
	@Autowired//(required=false)
	FabrykaPsow fabrykaDzikichPsow;

	@Override
	public void przyjmij() {
		System.out.println("Nowy pies w rezerwacie dzikich stworzen: " + fabrykaDzikichPsow.wyprodukuj());
	}

}
