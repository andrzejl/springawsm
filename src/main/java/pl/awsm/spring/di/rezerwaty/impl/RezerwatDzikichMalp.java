package pl.awsm.spring.di.rezerwaty.impl;

import pl.awsm.spring.di.fabryki.FabrykaDzikichStworzen;
import pl.awsm.spring.di.rezerwaty.RezerwatDzikichStworzen;

public class RezerwatDzikichMalp implements RezerwatDzikichStworzen {
	
	private FabrykaDzikichStworzen fabrykaMalp;

	@Override
	public void przyjmij() {
		System.out.println("Nowa malpa w rezerwacie dzikich stworzen: " + fabrykaMalp.wyprodukuj());
	}
	
	public void setFabrykaMalp(FabrykaDzikichStworzen fabrykaMalp) {
		this.fabrykaMalp = fabrykaMalp;
	}
	
	public FabrykaDzikichStworzen getFabrykaMalp() {
		return fabrykaMalp;
	}
}